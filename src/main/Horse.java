package main;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Женя on 09.10.2014.
 */
public class Horse extends Point {

    private static final int[] movesX = new int[]{-1, 1, 2, 2, 1, -1, -2, -2};
    private static final int[] movesY = new int[]{2, 2, 1, -1, -2, -2, -1, 1};
    private Horse parent;
    private int weight = 0;

    List<Horse> history;

    public Horse(int x, int y, Horse parent) {
        super(x, y);

        if (parent == null) {
            this.history = new ArrayList<Horse>();
        } else {
            this.parent = parent;
            this.history = parent.getHistory();
            weight = history.size();
        }
        history.add(this);
    }

    public List<Horse> getHistory() {
        return history;
    }

    public void setHistory(List<Horse> history) {
        this.history = history;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Horse getParent() {
        return parent;
    }

    public void setParent(Horse parent) {
        this.parent = parent;
    }

    public void addWeight(int weight) {
        this.weight += weight;
    }

    public Horse nextRandom(int[][] desk) {
        int size = desk.length;
        int newX;
        int newY;

        do {
            int i = (int) (Math.random() * size);

            newX = this.x + movesX[i];
            newY = this.y + movesY[i];
        }
        while (newX == parent.x && newY == parent.y || newX >= size && newY >= size);

        return new Horse(newX, newY, this);
    }

    public Horse nextRand(int[][] desk) {
        int size = desk.length;
        int newX = 0;
        int newY = 0;
        double[] pheromones = new double[movesX.length];
        int sumPher = 0;
        double rand = Math.random();
        double p = 0;

        for (int i = 0; i < movesX.length; i++) {

            newX = this.x + movesX[i];
            newY = this.y + movesY[i];

            if (newX >= 0 && newY >= 0 && newX < size && newY < size && (parent == null || newX != parent.x && newY != parent.y)) {
                pheromones[i] = desk[this.x + movesX[i]][this.y + movesY[i]];
                sumPher += pheromones[i];
            }
        }

        for (int i = 0; i < movesX.length; i++) {
            p += pheromones[i] / sumPher;
            if (p > rand) {
                newX = this.x + movesX[i];
                newY = this.y + movesY[i];

                Horse horse = new Horse(newX, newY, this);

                return horse;
            }
        }

        return null;
    }
}
