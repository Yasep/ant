package main;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Женя on 09.10.2014.
 */
public class King extends Point {

    private static final int[] movesX = new int[]{-1, 0, 1, 1, 1, 0, -1, -1};
    private static final int[] movesY = new int[]{1, 1, 1, 0, -1, -1, -1, 0};
    private int deskSize;
    private int weight = 0;
    int[][] weightDesk;

    public King(int x, int y, int deskSize) {
        super(x, y);
        this.deskSize = deskSize;
        this.weightDesk = this.generateWeightDesk(deskSize);
    }

    public King(int x, int y, int deskSize, int weight) {
        super(x, y);
        this.deskSize = deskSize;
        this.weight = weight;
    }

    public int[][] getWeightDesk() {
        return weightDesk;
    }

    public void setWeightDesk(int[][] weightDesk) {
        this.weightDesk = weightDesk;
    }

//    public King next(int[][] desk, Queue<King> queue) {
//        int size = movesX.length;
//
//        for (int i = 0; i < size; i++) {
//
//            int newX = this.x + movesX[i];
//            int newY = this.y + movesY[i];
//
//            if (newX >= 0 && newY >= 0 && newX < size && newY < size && desk[newX][newY] == -1) {
//                queue.add(new King(newX, newY, deskSize, false));
//            }
//        }
//        return null;
//    }

    public int[][] generateWeightDesk(int size) {
        int desk[][] = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                desk[i][j] = -1;
            }
        }
        Queue<King> queue = new LinkedList<King>();
        int weight = 0;
        desk[this.x][this.y] = 0;
        queue.add(this);

        while (!queue.isEmpty()) {
            King cur = queue.poll();
            for (int i = 0; i < movesX.length; i++) {

                int newX = cur.x + movesX[i];
                int newY = cur.y + movesY[i];

                if (newX >= 0 && newY >= 0 && newX < size && newY < size && desk[newX][newY] == -1) {
                    queue.add(new King(newX, newY, deskSize, cur.weight + 1));
                    desk[newX][newY] = cur.weight + 1;
                }
            }
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(desk[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        return desk;

    }
}
