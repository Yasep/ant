package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by desperado on 08.10.2014.
 */
public class Main {

    public static void main(String[] args) {

        new Main().run();
    }

    private void run() {

        int size = 10;
        int desk[][] = new int[size][size];
        double endChance = 0.8;
        int teamSize = 10;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                desk[i][j] = 1;
            }
        }

        King k1 = new King(0, 0, size);
        King k2 = new King(3, 5, size);
        King k3 = new King(7, 2, size);
        List<King> kings = new ArrayList<King>();

        kings.add(k1);
        kings.add(k2);
        kings.add(k3);

        Point t = new Horse(8, 6, null);
        List<Horse> horsesInTarget = new ArrayList<Horse>();
        List<Horse> teamInTarget = new ArrayList<Horse>();

        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < size; j++) {
                //generate horses team
                List<Horse> horses = new ArrayList<Horse>();

                for (int k = 0; k < teamSize; k++) {
                    horses.add(new Horse(1, 8, null));
                }

                for (Horse horse : horses) {
                    boolean toContinue = true;
                    boolean afterTarget = false;
                    int[] kingsMinWeights = new int[kings.size()];
                    int kingsWeightsSum = 0;

                    for (int k = 0; k < kings.size(); k++) {
                        kingsMinWeights[k] = Integer.MAX_VALUE;
                    }

                    while (toContinue) {
                        horse = horse.nextRand(desk);

                        for (int k = 0; k < kings.size(); k++) {
                            int kingDesk[][] = kings.get(k).getWeightDesk();
                            int weight = kingDesk[horse.x][horse.y];

                            if (weight < kingsMinWeights[k]) {
                                kingsMinWeights[k] = weight;
                            }
                        }

                        if (t.equals(horse)) {
                            toContinue = Math.random() < endChance ? false : true;
                            afterTarget = true;
                        }

                        if (afterTarget) {
                            if (horse.getHistory().size() == 3 * size) {
                                for (Horse h : horse.getHistory()) {
                                    if (h.x == horse.x && h.y == horse.y) {
                                        horse = h;
                                    }
                                }
                                toContinue = false;
                            }
                        }
                    }

                    for (int k = 0; k < kings.size(); k++) {
                        kingsWeightsSum += kingsMinWeights[k];
                    }

                    horse.addWeight(kingsWeightsSum);
                    teamInTarget.add(horse);
                }
                for (Horse horse : teamInTarget) {
                    markPheromones(desk, horse);
                }
            }
            horsesInTarget.addAll(teamInTarget);
        }

        int min = Integer.MAX_VALUE;
        int[] steps = new int[horsesInTarget.size()];

        for (int i = 0; i < steps.length; i++) {
            steps[i] = horsesInTarget.get(i).getWeight();
        }

        Arrays.sort(steps);

        for (int i = 0; i < steps.length; i++) {
            System.out.println(steps[i]);
        }

        for (Horse ho : horsesInTarget) {
            if (ho.getWeight() <17) {
                ho.getHistory();
            }
        }

        System.out.println("finish");

    }

    private void markPheromones(int[][] desk, Horse horse) {
// TODO maybe weight instead of history
        for (Horse h : horse.getHistory()) {
            if (horse.getWeight() != 0) {
                desk[h.x][h.y] += desk.length / horse.getHistory().size();
            }
        }
    }

}
