package main;

/**
 * Created by Женя on 09.10.2014.
 */
public abstract class Point {

    protected int x;
    protected int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Point other) {
        return this.x == other.x && this.y == other.y;
    }

    public boolean equals(Point... others) {
        for (Point point : others) {
            if (!this.equals(others)) {
                return false;
            }
        }
        return true;
    }
}
